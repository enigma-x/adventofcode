#!/usr/bin/python3

forward = int(0)
depth = int(0)


f = open("input.txt", "r")
data = list(f)

for i in range(0,len(data)):
	dataline = data[i]
	instruction = dataline.split()
	if instruction[0] == 'forward':
		forward = forward + int(instruction[1])
	if instruction[0] == 'up':
		depth = depth - int(instruction[1])
	if instruction[0] == 'down':
		depth = depth + int(instruction[1])			
	

print(int(forward)*int(depth))
