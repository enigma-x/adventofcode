#!/usr/bin/python3

forward = int(0)
depth = int(0)
downaim = int(0)

f = open("input.txt", "r")
data = list(f)

for i in range(0,len(data)):
	dataline = data[i]
	instruction = dataline.split()
	if instruction[0] == 'forward':
		forward = forward + int(instruction[1])
		if downaim != 0:
                	depth = depth + (int(instruction[1])*int(downaim))
	if instruction[0] == 'up':
		downaim = downaim - int(instruction[1])
	if instruction[0] == 'down':
		downaim = downaim + int(instruction[1])

	print("Forward = %s | Downaim = %s | Depth = %s" % (forward,downaim,depth))	

print(int(forward)*int(depth))
