#!/usr/bin/python3
import math

increase = int(0)
cur = int(0)
prev = int(0)
pos = int(0)
posmax = int(0)

f = open("input.txt", "r")

valuelist = list(f)
listlength = len(valuelist)
posmax = (int(listlength) - int(listlength)%3)
iterations = math.trunc(int(listlength)/3)

for i in range(0,posmax):
	val = int(valuelist[pos])+int(valuelist[int(pos+1)])+int(valuelist[int(pos+2)])
	if pos == 0:
		cur = val
	else:
		prev = cur
		cur = val
		if int(cur) > int(prev):
			increase += 1
	pos += 1

print("Increased:",increase)
