#!/usr/bin/python3

#set vars
linenum = int(0)
increase = int(0)
cur = int(0)
prev = int(0)

# open file
fo = open("input.txt", "r")

while True:
	val = fo.readline()
	if linenum == 0:
		cur = val
	else:
		prev = cur
		cur = val

		if val != "":
			if int(cur) > int(prev):
				increase += 1
				print(val,"1")
			else:
				print(val,"0")
	

	linenum += 1

	if not val:
		break

print("Amount of increases:",increase)
print("Number of lines:",linenum)



