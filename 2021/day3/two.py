#!/usr/bin/python3
from collections import Counter

def get_most_frequent(List,pos):
	chklist = []
	for i in range(0,len(List)):
		dataline = List[i]
		chklist.append(dataline[pos])
	c = Counter(chklist)
	if int(c["0"]) == int(c["1"]):
		return 1
	else:
		return c.most_common(1)[0][0]

def get_least_frequent(List,pos):
	most = int(get_most_frequent(List,pos))
	if most == 1:
		least = 0
	else:
		least = 1
	return least

def filter_list(List,char,pos):
	filtered_list = list(filter(lambda x: x[pos] == str(char), List))
	return filtered_list

def preload():
	f = open("input", "r")
	data = list(f)
	return data

def get_oxygen():
	data = preload()
	pos = 0
	while len(data) > 1:
		mf = get_most_frequent(data,pos)
		flist = filter_list(data,mf,pos)
		data = flist
		pos += 1
	else:
		oxygenbin = data[0]
	return oxygenbin

def get_co2():
	data = preload()
	pos = 0
	while len(data) > 1:
		lf = get_least_frequent(data,pos)
		flist = filter_list(data,lf,pos)
		data = flist
		pos += 1
	else:
		co2bin = data[0]
	return co2bin


def main():
	oxygen = get_oxygen()
	
	co2 = get_co2()

	value = (int(oxygen,2)*int(co2,2))
	return value

if __name__ == "__main__":
    print(main())

