#!/usr/bin/python3
from collections import Counter

def most_frequent(List):
    c = Counter(List)
    return c.most_common(1)[0][0]

f = open("input", "r")
data = list(f)

bitdict = {}
gammabits = ""
epsilonbits = ""

for i in range(0,12):
	bitdict[i] = []

for i in range(0,len(data)):
	dataline = data[i]

	for j in range(0,12):
		bitdict[j].append(dataline[j])


for k in range(0,12):

	mf = int(most_frequent(bitdict[k]))
	if mf == 0:
		gammabits = gammabits + "0"
		epsilonbits = epsilonbits + "1"
	else:
		gammabits = gammabits + "1"
		epsilonbits = epsilonbits + "0"

print(int(gammabits,2)*int(epsilonbits,2))

print(gammabits)
